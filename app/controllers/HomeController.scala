package controllers

import javax.inject._
import play.api.mvc._
import anorm._
import play.api.db.Database
import play.api.libs.json._
import scala.concurrent.Future

case class ParserClass(id: String, subject: String, body: String, status: String)
case class ParserClassRead(subject: String, body: String, status: String)

@Singleton
class HomeController @Inject()(cc: ControllerComponents, db:Database) extends AbstractController(cc) {

  implicit val conn = db.getConnection()
  implicit val parserClassWrite = Json.writes[ParserClass]
  implicit val parserClassRead = Json.reads[ParserClassRead]

  implicit def transformToJson(data: List[JsValue]): JsValue = JsArray(data)

  implicit def mapToParserClassTuple(obj: Map[String, Any]): Tuple4[String, String, String, String] = {
    obj.mapValues(_.toString).values.toList match {
      case List(id: String, subject: String, body: String, status: String) => (id, subject, body, status)
    }
  }

  val parser: RowParser[Map[String, Any]] =
    SqlParser.folder(Map.empty[String, Any]) { (map, value, meta) =>
      Right(map + (meta.column.qualified -> value))
    }

  def getAll() = Action {
    val data: List[Map[String, Any]] = SQL("select * from content").as(parser.*)
    val parserClassList: List[ParserClass] = data.map(d => ParserClass.tupled(d))
    val json: JsValue = parserClassList.map(d => Json.toJson(d))
    Ok(json)
  }

  def getOne(id: String) = Action {
    val data: Map[String, Any] = SQL(s"select * from content where id = $id").as(parser.*).headOption match {
      case Some(d) => d
      case None => Map()
    }
    val parserClass: ParserClass = ParserClass.tupled(data)
    val json: JsValue = Json.toJson(parserClass)
    Ok(json)
  }

  def create = Action.async(parse.json) { req =>
    Json.fromJson[ParserClassRead](req.body) match {
      case JsSuccess(value, _) => {
        SQL(s"INSERT INTO Content(subject, body, status) values ('${value.subject}', '${value.body}', '${value.status}');").executeInsert()
        Future.successful(Ok("Successfully created"))
      }
      case _ => Future.successful(BadRequest("Parsed Error"))
    }
  }

  def update(id: String) = Action.async(parse.json) { req =>
    Json.fromJson[ParserClassRead](req.body) match {
      case JsSuccess(value, _) => {
        SQL(
          s"UPDATE content " +
            s"SET subject='${value.subject}', body='${value.body}', status='${value.status}' " +
            s"WHERE id='${id}'"
        ).executeUpdate()
        Future.successful(Ok("Successfully updated"))
      }
      case _ => Future.successful(BadRequest("Parsed Error"))
    }
  }

  def remove(id: String) = Action {
    SQL(s"DELETE FROM content WHERE id= $id").executeUpdate() match {
      case 1 => Ok("Successfully deleted")
      case _ => BadRequest("No data")
    }
  }
}
