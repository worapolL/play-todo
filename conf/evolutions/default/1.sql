# Users schema

# --- !Ups

CREATE TABLE Content (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    subject varchar(255) NOT NULL,
    body varchar(255) NOT NULL,
    status varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO Content values (1,'Hello World','Test play', 'done');
INSERT INTO Content values (2,'Hello World','Test play', 'done');
# --- !Downs

DROP TABLE Content;
