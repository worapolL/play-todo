name := """todolist"""
organization := "todolist"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += evolutions
libraryDependencies += jdbc
libraryDependencies += "com.h2database" % "h2" % "1.4.192"
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % Test
libraryDependencies += "com.typesafe.play" %% "anorm" % "2.5.3"
//libraryDependencies += "postgresql" % "postgresql" % "9.1-901-1.jdbc4"

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "todolist.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "todolist.binders._"
